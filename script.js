
var list =      [],
    crntCounter=0,
    addCounter = 0,
    rmvCounter = 0,
    add =       document.getElementById('add'),
    newValue =  document.getElementById('newValue'),
    tasks =     document.getElementById('tasks'),
    reset =     document.getElementById('reset');


add.addEventListener('click',addItem);
tasks.addEventListener('click',remove);
reset.addEventListener('click',resetting);

function addItem () {
  if(newValue.value !== '') {
      list.push(newValue.value);
      listing();
      newValue.value = '';
      addCounter++ ;
      crntCounter++;
  }
}


function listing () {
    tasks.innerHTML='';
    for (var I = 0; I < list.length; I++)
      {
         var y = "<li id="+I+">" + list[I] + "</li>";
         document.getElementById("tasks").innerHTML += y;
      }
}


function remove(e) {
    var li = e.target;
    var ol = li.parentElement;
    ol.removeChild(li);
    removeFromArray(e.target.id)
}

function removeFromArray(locate){
    list.splice(locate,1);
    listing();
    rmvCounter++ ;
    crntCounter-- ;
}

function resetting (){
    list=[];
    crntCounter=addCounter=rmvCounter=0;
    tasks.innerHTML= '';
    newValue.value ='';
}

function assert (condition, message) {
    if (!condition) {
        console.log('ERROR - ' + message);
        alert('Check the ' + message)

    }else {
        console.log('CHECKED - ' + message);
    }
}
